import init, {
  open_invite,
  handle_new_lox_credential,
  trust_promotion,
  handle_trust_promotion,
  trust_migration,
  handle_trust_migration,
  level_up,
  handle_level_up,
  issue_invite,
  handle_issue_invite,
  prepare_invite,
  redeem_invite,
  handle_redeem_invite,
  check_blockage,
  handle_check_blockage,
  blockage_migration,
  handle_blockage_migration,
  set_panic_hook, get_last_upgrade_time, get_trust_level, get_invites_remaining, get_next_unlock, get_received_invite_expiry, get_bridgelines_from_bucket} from "./pkg/lox_wasm.js";
let pubkeys = await simple_request("/pubkeys");
console.log("Got pubkeys: " + pubkeys);
let constants = await simple_request("/constants");
console.log("Got constants: " + constants);

// Get Lox Invitation
let requested_invite = await init().then(() => {
  set_panic_hook();
  let requested_invite = request_open_invite().then((token) => {
    return open_invite(token);
  });
  return requested_invite;
});
console.log("Got request and state: "+requested_invite);

// Redeem Lox Invitation for an Open Invitation Lox Credential
// Trust Level 0
let open_lox_cred = await init().then(() => {
  set_panic_hook();
  let cred = requested_cred("/openreq", requested_invite).then((response) => {
    console.log("Got new Level 0 Lox Credential: " + response);
    return handle_new_lox_credential(requested_invite, response, pubkeys);
  });
  return cred;
});
let unlock_info = get_next_unlock(constants, open_lox_cred);
console.log("Unlock info: "+unlock_info);
get_last_upgrade_time(open_lox_cred);
get_trust_level(open_lox_cred);
get_invites_remaining(open_lox_cred);

let encrypted_table = await simple_request("/reachability");
let info_five = get_bridgelines_from_bucket(open_lox_cred, encrypted_table);
console.log("Bridgelines available: "+info_five);
let requested_trust_promo = trust_promotion(open_lox_cred, pubkeys);

// Get Migration credential for Trust Promotion from Trust Level 0 -> 1
let trust_promo_cred = await init().then(() => {
  set_panic_hook();
  let cred = requested_cred("/trustpromo", requested_trust_promo).then((response)=> {
    console.log("Got Migration Credential for Trust Promotion: " + response);
    return handle_trust_promotion(requested_trust_promo, response);
  });
  return cred;
  });


let requested_trust_migration = trust_migration(open_lox_cred, trust_promo_cred, pubkeys);

// Trust Promotion from Trust Level 0 -> 1
let lox_cred = await init().then(() => {
  set_panic_hook();
  let cred = requested_cred("/trustmig", requested_trust_migration).then((response)=> {
    console.log("Got new Level 1 Lox Credential: " + response);
    return handle_trust_migration(requested_trust_migration, response, pubkeys);
  });
  return cred;
  });

encrypted_table = await simple_request("/reachability");
console.log("Got Encrypted Table: " + encrypted_table);
let requested_level_two = level_up(lox_cred, encrypted_table, pubkeys);

// Level Up to Trust Level 2
lox_cred = await init().then(() => {
  set_panic_hook();
  let cred = requested_cred("/levelup", requested_level_two).then((response)=> {
    console.log("Got new Level 2 Lox Credential: " + response);
    return handle_level_up(requested_level_two, response, pubkeys);
  });
  return cred;
  });

// Update reachability cred
  encrypted_table = await simple_request("/reachability");
  console.log("Got Encrypted Table: " + encrypted_table);
  let requested_level_three = level_up(lox_cred, encrypted_table, pubkeys);

// Level Up to Trust Level 3
  lox_cred = await init().then(() => {
    set_panic_hook();
    let cred = requested_cred("/levelup", requested_level_three).then((response)=> {
      console.log("Got new Level 3 Lox Credential: " + response);
      return handle_level_up(requested_level_three, response, pubkeys);
    });
    return cred;
    });

let info_three = get_invites_remaining(lox_cred);
console.log("Last upgrade time: "+info_three);

// Update reachability cred
encrypted_table = await simple_request("/reachability");
console.log("Got Encrypted Table: " + encrypted_table);
let requested_level_four = level_up(lox_cred, encrypted_table, pubkeys);

// Level Up to Trust Level 4
lox_cred = await init().then(() => {
  set_panic_hook();
  let cred = requested_cred("/levelup", requested_level_four).then((response)=> {
    console.log("Got new Level 4 Lox Credential: " + response);
    return handle_level_up(requested_level_four, response, pubkeys);
  });
  return cred;
  });

// Update reachability cred
encrypted_table = await simple_request("/reachability");
console.log("Got Encrypted Table: " + encrypted_table);
let requested_issue_invitation = issue_invite(lox_cred, encrypted_table, pubkeys);

// Issue an Invitation cred
lox_cred = await init().then(() => {
  set_panic_hook();
  let cred = requested_cred("/issueinvite", requested_issue_invitation).then((response)=> {
    console.log("Got new Invite and Lox Credential: " + response);
    return handle_issue_invite(requested_issue_invitation, response, pubkeys);
  });
  return cred;
  });

let prepared_invitation = prepare_invite(lox_cred);
// Trusted Invitation Request
let requested_invitation = redeem_invite(prepared_invitation, pubkeys);
// Redeem an Invitation cred

let info_four = get_received_invite_expiry(lox_cred);
console.log("Last upgrade time: "+info_four);

let lox_cred_from_invite = await init().then(() => {
  set_panic_hook();
  let cred = requested_cred("/redeem", requested_invitation).then((response)=> {
    console.log("Got new Trusted Lox Credential Invite: " + response);
    return handle_redeem_invite(requested_invitation, response, pubkeys);
  });
  return cred;
  });

  let requested_check_blockage = check_blockage(lox_cred, pubkeys);

  // Check whether or not a bucket is blocked
  let check_migration_cred = await init().then(() => {
    set_panic_hook();
    let cred = requested_cred("/checkblockage", requested_check_blockage).then((response)=> {
      console.log("Got check blockage Migration Credential: " + response);
      return handle_check_blockage(requested_check_blockage, response);
    });
    return cred;
    });

let requested_blockage_migration = blockage_migration(lox_cred, check_migration_cred, pubkeys);

  // Migrate to a new unblocked bridge
  lox_cred = await init().then(() => {
    set_panic_hook();
    let cred = requested_cred("/blockagemigration", requested_blockage_migration).then((response)=> {
      console.log("Got Lox Credential for new bucket: " + response);
      return handle_blockage_migration(requested_blockage_migration, response, pubkeys);
    });
    return cred;
    });


function requested_cred(command, requested) {
  return new Promise((fulfill, reject) => {
  let req = JSON.parse(requested);
    loxServerPostRequest(command, req.request).then((response) => {
      fulfill(JSON.stringify(response));
      return;
    }).catch(() => {
      console.log("Error requesting new Lox credential from server");
      reject();
    });
  });
}

function request_open_invite() {
  return new Promise((fulfill, reject) => {
    loxServerPostRequest("/invite", null).then((response) => {
      console.log("Got invitation token: " + response.invite);
      fulfill(JSON.stringify(response));
      return;
    }).catch(() => {
      console.log("Error requesting open invite from Lox server");
      reject();
    });
  });
}

function simple_request(requested) {
  return new Promise((fulfill, reject) => {
    loxServerPostRequest(requested, null).then((response) => {
      fulfill(JSON.stringify(response));
      return;
    }).catch(() => {
      console.log("Error making simple request: " + requested);
      reject();
    });
  });
}

function loxServerPostRequest(data, payload) {
  return new Promise((fulfill, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      if (xhr.DONE !== xhr.readyState) {
        return;
      }
      if (xhr.status !== 200) {
        console.log("Error. Status code: "+xhr.status);
        console.log(xhr);
        reject();
        return;
      }
      const response = JSON.parse(xhr.responseText);
      fulfill(response);
      return;
    };
    try {
      xhr.open('POST', "http://localhost:8001"+data, true)
      xhr.setRequestHeader("Content-Type", "application/json");
    } catch (err) {
      console.log("Error connecting to lox bridge db");
      reject();
      return;
    }
    xhr.send(JSON.stringify(payload));
  });
}

// The correct key should be matched against a public commit to the key to
// verify that the key issuer is in fact the correct Bridge Authority
function loxKeyRequest(key_type) {
  return new Promise((fulfull, reject) => {


  })
}
